# Generated by Django 2.1.2 on 2018-10-24 12:07

from django.conf import settings
import django.core.files.storage
from django.db import migrations, models
import django.db.models.deletion
import secup.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='SecureUpload',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uploader_email', models.EmailField(blank=True, max_length=254, null=True, verbose_name='Uploader Email')),
                ('file', models.FileField(blank=True, null=True, storage=django.core.files.storage.FileSystemStorage(base_url='/secup/secure-media/', location='/Users/macbook/Desktop/lequest/fileshare/secure-media/'), upload_to=secup.models.file_name, verbose_name='File')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Description')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='Created At')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='Updated At')),
                ('receiver', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='receiver', to=settings.AUTH_USER_MODEL, verbose_name='Receiver')),
                ('uploader', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='uploader', to=settings.AUTH_USER_MODEL, verbose_name='Uploader')),
            ],
            options={
                'verbose_name': 'Secure Uplaod',
                'verbose_name_plural': 'Secure Uploads',
                'ordering': ['-created_at'],
            },
        ),
    ]
